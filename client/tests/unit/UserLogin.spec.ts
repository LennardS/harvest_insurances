import { expect } from 'chai';
import { shallowMount } from '@vue/test-utils';
import UserLogin from '@/views/UserLogin.vue';
import moxios from 'moxios';

describe('Tests behaviour upon user login or failed validation', () => {
    beforeEach(function() {
      moxios.install();
    });

    afterEach(function() {
      moxios.uninstall();
    });

    it('it should disable button after form submission and while the axios request is active', () => {
        const wrapper = shallowMount(UserLogin);
        expect(wrapper.vm.$data.isDisabled).equal(false);

        wrapper.find('[type=\'submit\']').trigger('submit.prevent');

        expect(wrapper.vm.$data.isDisabled).equal(true);
      });

    it('should add errormessages and not accept user upon failed validation on the server', (done) => {
    const wrapper = shallowMount(UserLogin);
    expect(wrapper.vm.$data.isDisabled).equal(false);

    moxios.withMock(function() {
      wrapper.find('[type=\'submit\']').trigger('submit.prevent');
      moxios.wait(function() {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 401,
          response: {
            timestamp: 1574346439947,
            status: 401,
            error: 'Unauthorized',
            message: 'Unauthorized',
            path: '/user/session',
        },
        }).then(function() {
          done();

          expect(wrapper.vm.$data.isDisabled).equal(false);
          expect(wrapper.props().errorList.length).equals(1);
        });
      });
    });
    });

    it('should set authentication token after succesful login', (done) => {
      const wrapper = shallowMount(UserLogin);
      expect(wrapper.vm.$data.isDisabled).equal(false);

      moxios.withMock(function() {
        wrapper.find('[type=\'submit\']').trigger('submit.prevent');
        moxios.wait(function() {
          const request = moxios.requests.mostRecent();
          request.respondWith({
            status: 200,
            response: {
              token: 'eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg',
          },
          }).then(function() {
            done();

            expect(wrapper.vm.$data.isDisabled).equal(false);
            expect(wrapper.props().errorList.length).equals(0);
          });
        });
      });
      });

    it('should log 1 errormessage after no axios response', (done) => {
    const wrapper = shallowMount(UserLogin);

    expect(wrapper.vm.$data.isDisabled).equal(false);

    wrapper.find('[type=\'submit\']').trigger('submit.prevent');
    done();

    expect(wrapper.props().errorList.length).equals(1);
    expect(wrapper.vm.$data.isDisabled).equal(false);
    });
});
