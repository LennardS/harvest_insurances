import AxiosConfig from '@/types/AxiosConfig';
import moxios from 'moxios';

describe('Tests behaviour upon user login or failed validation', () => {
    beforeEach(function() {
        moxios.install();
      });

    afterEach(function() {
        moxios.uninstall();
      });

    it('should request a csrf token on initialization', (done) => {
        moxios.withMock(function() {

        const axiosConfig = new AxiosConfig();

        moxios.wait(function() {
            const request = moxios.requests.mostRecent();
            request.respondWith({
            status: 200,
            }).then(function() {
            done();
            });
        });
        });
    });
});
