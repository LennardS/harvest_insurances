import { expect } from 'chai';
import { mount, shallowMount } from '@vue/test-utils';
import UserRegistration from '@/views/UserRegistration.vue';
import CountryList from '@/components/CountryList.vue';
import moxios from 'moxios';
import sinon from 'ts-sinon';

describe('Tests user registration and validation behaviour', () => {
  beforeEach(function() {
    moxios.install();
  });

  afterEach(function() {
    moxios.uninstall();
  });

  it('should disable button after clicking form submission and while the axios request is active', () => {
      const wrapper = shallowMount(UserRegistration);
      expect(wrapper.vm.$data.isDisabled).equal(false);

      wrapper.find('[type=\'submit\']').trigger('submit.prevent');

      expect(wrapper.vm.$data.isDisabled).equal(true);
    });

  it('should not contain any errors and accept user upon succesful axios request', (done) => {
        const wrapper = shallowMount(UserRegistration);
        expect(wrapper.vm.$data.isDisabled).equal(false);

        moxios.withMock(function() {
          wrapper.find('[type=\'submit\']').trigger('submit.prevent');
          moxios.wait(function() {
            const request = moxios.requests.mostRecent();
            request.respondWith({
              status: 201,
              response: {
                bsn: '123456788',
                fullName: 'Test TestUser',
                address: 'Addres 54',
                emailAddress: 'test@mail.org',
                country: 'BD',
                zipCode: '0000BB',
                password: '$2a$10$X9RGgD1MWQWDbZAA6rckqOupfRlyN.ZJZv6gdR/x8X.y7gOtEpsXC',
              },
            }).then(function() {
              done();

              expect(wrapper.vm.$data.isDisabled).equal(false);
              expect(wrapper.props().errorList.length).equals(0);
            });
          });
        });
    });

  it('should add errormessages and not accept user upon failed validation on the server', (done) => {
    const wrapper = shallowMount(UserRegistration);
    expect(wrapper.vm.$data.isDisabled).equal(false);

    moxios.withMock(function() {
      wrapper.find('[type=\'submit\']').trigger('submit.prevent');
      moxios.wait(function() {
        const request = moxios.requests.mostRecent();
        request.respondWith({
          status: 400,
          response: {
            httpStatus: 'BAD_REQUEST',
            timestamp: '2019-11-07T14:59:08.0651821',
            message: 'Validatie error',
            details: 'More detailed description is in the message below',
            errorMessages: [
              'De ingevoerde postcode is niet juist',
              'Het ingevoerde land is niet juist',
              'Het ingevoerde adres is niet juist',
              'Het wachtwoord moet bestaan uit tenminste 8 karakters, waarvan minstens 1 cijfer, 1 hoofdletter en 1 teken',
              'Vul een geldig emailadres  in',
              'Vul een juiste \'volledige naam\' in',
              'Het ingevoerde bsn-nummer is niet juist',
            ],
          },
        }).then(function() {
          done();

          expect(wrapper.vm.$data.isDisabled).equal(false);
          expect(wrapper.props().errorList.length).equals(7);
        });
      });
    });
  });

  it('should log 1 errormessage after no axios response', (done) => {
    const wrapper = shallowMount(UserRegistration);
    expect(wrapper.vm.$data.isDisabled).equal(false);

    wrapper.find('[type=\'submit\']').trigger('submit.prevent');
    done();

    expect(wrapper.props().errorList.length).equals(1);
    expect(wrapper.vm.$data.userIsAccepted).equal(false);
    expect(wrapper.vm.$data.isDisabled).equal(false);
    });

  it('should render CountryList component', () => {
    const wrapper = mount(UserRegistration);
    const spy = sinon.spy(wrapper.vm, 'setCountryListener');

    wrapper.find('[type=\'submit\']').trigger('submit.prevent');

    expect(wrapper.html()).to.contain('<b-form-select');
    expect(wrapper.find(CountryList).exists()).to.be.true;
    expect(spy.called).to.be.false;
  });
});
