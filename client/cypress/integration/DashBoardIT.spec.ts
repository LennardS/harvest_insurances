context('Tests integration between dashboard and children tiles', () => {
    let authTokenStr = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyOTk5fQ.E8Who8qCcSxPNyu0JKdhnGwFX3MPJevaJQNn4Ca_uWE';

    beforeEach(() => {
    });

    it('should request tiles upon visit and contain the right payload', () => {
        //a similar cookie is set uppon login
        cy.setCookie('authToken', authTokenStr)
        cy.server()
        cy.route('http://localhost:8080/user/tiles', [
            
            [{"userLoginData":{},"tileType":"USER_PROFILE_TILE","userProfileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"},"tileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"}},{"userLoginData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"},"tileType":"USER_LOGIN_TILE","tileData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"}}]
            
        ]).as('getTiles')

        cy.visit('http://localhost:8081/dashboard')

        cy.wait('@getTiles').its('response.body')
            .should('deep.equal', [[{"userLoginData":{},"tileType":"USER_PROFILE_TILE","userProfileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"},"tileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"}},{"userLoginData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"},"tileType":"USER_LOGIN_TILE","tileData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"}}]])
    })

    it('should request tiles upon visit and not get content', () => {
        cy.server()
        cy.route('http://localhost:8080/user/tiles').as('getTiles')

        cy.visit('http://localhost:8081/dashboard')

         cy.wait('@getTiles').its('response.body')
             .should('not.eq', [[{"userLoginData":{},"tileType":"USER_PROFILE_TILE","userProfileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"},"tileData":{"country":"BD","zipCode":"4401JE","address":"Fregatstraat 29","fullName":"Lennard S","bsn":"123456789"}},{"userLoginData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"},"tileType":"USER_LOGIN_TILE","tileData":{"emailAddress":"lennard.slabbekoorn@harvest.nl","password":"","bsn":"123456789"}}]]);
    })
});