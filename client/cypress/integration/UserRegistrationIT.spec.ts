context('Tests integration between UserRegistration.vue and CountryList.vue', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8081/register')
    });

    it('should check whether CountryList is rendered within UserRegistration.vue', () => {
        cy.get('.CountryList select').select('CG');
        cy.get('.CountryList select').select('GL');
        cy.get('.CountryList select').select('NL');
    });

    it('should submit last selected country to server', () => {
      cy.server()
      cy.route({
          method: 'POST',
          url: '/user'
        })
        .as('createUser')

      cy.get('.CountryList select').select('CG');
      cy.get('.CountryList select').select('GL');
      cy.get('.submitButton').click();

      cy.wait('@createUser')
      .its('request.body.country')
      .should('deep.equal', 'GL');
    })

    it('should submit the selected country and typed properties to server', () => {
      cy.server()
      cy.route({
          method: 'POST',
          url: '/user'
        })
        .as('createUser');
    
        cy.get('input[name="bsn"]').type('123456789')
        cy.get('input[name="fullName"]').type('Test test')
        cy.get('input[name="emailAddress"]').type('test@mail.nl')
        cy.get('input[name="address"]').type('street-test 23b')
        cy.get('input[name="zipCode"]').type('2222BB')
        cy.get('input[name="password"]').type('PassWorD123$')
        cy.get('.CountryList select').select('NL');
        cy.get('.submitButton').click();

        cy.wait('@createUser')
        .its('request.body')
        .should('include', {
          bsn:'123456789',
          fullName:'Test test',
          emailAddress:'test@mail.nl',
          address:'street-test 23b',
          zipCode:'2222BB',
          password:'PassWorD123$',
          country:'NL',
        });
    });

    it('should match snapshot', () => {
      cy.get('#user-register-container').snapshot();
    })
});