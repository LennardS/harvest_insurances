context('Tests integration between Navbar and the rest of the mounted components', () => {
    let authTokenStr = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyOTk5fQ.E8Who8qCcSxPNyu0JKdhnGwFX3MPJevaJQNn4Ca_uWE';

    beforeEach(() => {
      cy.visit('http://localhost:8081/login')
    });

    it('should update GUI and logout after a session cookie gets set', () => {
        //A similar cookie is set upon login
        cy.setCookie('authToken', authTokenStr)
        cy.visit('http://localhost:8081/login')

        cy.server()
        cy.route({
            method: 'POST',
            url: '/user/session/invalidate'
          })
          .as('logoutUser');

        cy.get('.dropdown-toggle').click()
        cy.get('.submitBtn').click()
        

        cy.wait('@logoutUser')
        .its('request.headers.Authorization')
        .should('include', authTokenStr);

        cy.url().should((url) => {
            expect(url).to.eq('http://localhost:8081/login')
          })

          //below code should be used in a end-to-end test
        // cy.request({
        //     method: 'POST',
        //     url: Cypress.env('serverUrlDev') + '/csrfToken',
        // }).then((response) => {
        //     expect(response.status).to.eq(200)
        //   })

        // cy.request({
        //     method: 'POST',
        //     url: Cypress.env('serverUrlDev') + '/user/session/invalidate',
        //     headers: {
        //         'Content-type': 'application/json',
        //         'Authorization': "Bearer " + cy.getCookie('authToken'),
        //     }
        //   }).then((response) => {
        //     expect(response.status).to.eq(200)
        //     expect(response.redirectedToUrl).to.eq('http://localhost:8081/login')
        //   })
    })

    it('Navbar should match snapshot when rendered', () => {
        cy.get('.HarvestNavBar').snapshot();
    });

    it('Navbar should match snapshot when logged in', () => {
        cy.setCookie('authToken', authTokenStr)
        cy.visit('http://localhost:8081/login')

        cy.get('.HarvestNavBar').snapshot();
    });

    it('Navbar login/logout dropdown should match snapshot when logged in', () => {
        cy.setCookie('authToken', authTokenStr)
        cy.visit('http://localhost:8081/login')
        cy.get('.dropdown-toggle').click()

        cy.get('.HarvestNavBar').snapshot();
    });
});