context('Tests integration between UserRegistration.vue and CountryList.vue', () => {
    beforeEach(() => {
      cy.visit('http://localhost:8081/login')
    });

    it('should submit login credentials to server', () => {
      cy.server()
      cy.route({
          method: 'POST',
          url: '/user/session'
        })
        .as('loginUser')

        cy.get('input[name="bsn"]').type('123456789')
        cy.get('input[name="password"]').type('PassWorD123$')
        cy.get('.submitButton').click();

      cy.wait('@loginUser')
      .its('request.body')
      .should('include', {
        userName:'123456789',
        password:'PassWorD123$',
      });
    })

    it('should submit last login credentials to server', () => {
      cy.server()
      cy.route({
          method: 'POST',
          url: '/user/session'
        })
        .as('loginUser');
    
        cy.get('input[name="bsn"]').type('123456789')
        cy.get('input[name="bsn"]').clear()
        cy.get('input[name="bsn"]').type('987654321')
        cy.get('input[name="password"]').type('PassWorD123$')
        cy.get('input[name="password"]').clear()
        cy.get('input[name="password"]').type('$321PassWorD')
        cy.get('.submitButton').click();

        cy.wait('@loginUser')
        .its('request.body')
        .should('include', {
          userName:'987654321',
          password:'$321PassWorD',
        });
    });

    it('should match snapshot when rendered', () => {
        cy.get('#user-login-container').snapshot();
    });
});