context('Tests if the app works well with Vue router', () => {

    it('should redirect to login page upon application startup', () => {
        cy.visit('http://localhost:8081/')
        cy.url().should((url) => {
            expect(url).to.eq('http://localhost:8081/login')
          })
    });

    it('should redirect to login page with router link', () => {
        cy.visit('http://localhost:8081/register')
        cy.get('#user-register-container').get('.router-link').click()
        cy.url().should((url) => {
            expect(url).to.eq('http://localhost:8081/login')
          })
        cy.get('#user-login-container');
    });

    it('should redirect to login page with router link', () => {
        cy.visit('http://localhost:8081/login')
        cy.get('#user-login-container').get('.router-link').click()
        cy.url().should((url) => {
            expect(url).to.eq('http://localhost:8081/register')
          })

          cy.get('#user-register-container');
    });
});