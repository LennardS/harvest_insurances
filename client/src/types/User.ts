export default class User {
    private bsn: string = '';
    private fullName: string = '';
    private address: string = '';
    private emailAddress: string = '';
    private country: string = '';
    private zipCode: string =  '';
    private password: string = '';

    constructor(init?: Partial<User>) {
        Object.assign(this, init);
    }

    public getBsn(): string {
        return this.bsn;
    }

    public setBsn(bsn: string) {
         this.bsn = bsn;
    }

    public getFullName(): string {
        return this.fullName;
    }

    public setFullName(fullName: string) {
        this.fullName = fullName;
    }

    public getAddress(): string {
        return this.address;
    }

    public setAddress(address: string) {
        this.address = address;
    }

    public getEmailAddress(): string {
        return this.emailAddress;
    }

    public setEmailAddress(emailAddress: string) {
         this.emailAddress = emailAddress;
    }

    public getCountry(): string {
        return this.country;
    }

    public setCountry(country: string) {
        this.country = country;
    }

    public getZipCode(): string {
        return this.zipCode;
    }

    public setZipCode(zipCode: string) {
        this.zipCode = zipCode;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(password: string) {
        this.password = password;
    }
}
