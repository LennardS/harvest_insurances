import axios from 'axios';

export default class AxiosConfig {
    constructor(init?: Partial<AxiosConfig>) {
        Object.assign(this, init);
        axios.defaults.withCredentials = true;
        this.setFirstCsrfToken();
    }

    private async setFirstCsrfToken() {
        // browser will automatically set csrf-token from cookie in server response
        await axios.get(process.env.VUE_APP_API_BASE_URL + `/csrfToken`);
    }
}
