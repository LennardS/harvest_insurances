export default class LoginModel {
    private userName: string = '';
    private password: string = '';

    constructor(init?: Partial<LoginModel>) {
        Object.assign(this, init);
    }

    public getUserName(): string {
        return this.userName;
    }

    public setUserName(userName: string) {
        this.userName = userName;
    }

    public getPassword(): string {
        return this.password;
    }

    public setPassword(password: string) {
        this.password = password;
    }
}
