import { TileTypeEnum }  from './TileTypeEnum';

export default abstract class BaseTileType {
    protected type: TileTypeEnum;
    protected tileData: Map<string, string>;

    constructor(type: TileTypeEnum, tileData: Map<string, string>) {
        this.type = type;
        this.tileData = tileData;
    }

    public getType(): TileTypeEnum {
        return this.type;
    }

    public getTileData(): Map<string, string> {
        return this.tileData;
    }
}
