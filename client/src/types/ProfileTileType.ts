import BaseTileType from './BaseTileType';
import { TileTypeEnum } from './TileTypeEnum';

export default class ProfileTiletype extends BaseTileType {

    constructor(type: TileTypeEnum, tileData: Map<string, string>) {
        super(type, tileData);
       }
}
