import BaseTileType from './BaseTileType';
import { TileTypeEnum } from './TileTypeEnum';

export default class LoginTileType extends BaseTileType {

    constructor(type: TileTypeEnum, tileData: Map<string, string>) {
        super(type, tileData);
       }
}
