import Vue from 'vue';
import VueRouter from 'vue-router';
import App from './App.vue';
import UserRegistration from './views/UserRegistration.vue';
import UserLogin from './views/UserLogin.vue';
import DashBoard from './views/DashBoard.vue';
import AxiosConfig from './types/AxiosConfig';

Vue.config.productionTip = false;
Vue.use(VueRouter);

const router = new VueRouter({
  mode: 'history',
  routes: [
    { path: '/', redirect: '/login', name: 'home'},
    { path: '/login', name: 'login', component: UserLogin },
    { path: '/register', name: 'app', component: UserRegistration },
    { path: '/dashboard', name: 'dashboard', component: DashBoard},
  ],
});

new Vue({
  router,
  render: (h) => h(App),
  created() {
    // set global configurations
    new AxiosConfig;
  },
}).$mount('#app');
