# Harvest insurances
HTTPS: https://gitlab.com/LennardS/harvest_insurances.git
SSH: git@gitlab.com:LennardS/harvest_insurances.git
*Branch development is currently the most up-to-date (stable) build*

The project consists of three major parts: 
  - Spring Boot REST API
  - Vue CLI + Typescript frontend
  - PostgreSQL database

## Local environment
---
### Prerequisites
  - Node.Js 6.9 to guarantee stabibility 
  - Maven 3.6.2 to guarantee stabibility
  - Java JDK 8 to guarantee stabibility
  - Java JRE
  - PostgreSQL to guarantee stabibility
  - Ensure **ocalhost:8080** and **localhost:8081** are accessible
 
### Building and running of the application
**Installation Database**
Create an empty database called: 'harvest_insurances' on a local PostgreSQL server
The user and password to this database should be:
- user: 'postgres'
- password: 'mysecretpassword'

**Build and run REST API**
It is possible to build and run the Spring Boot REST API either with the command line with Maven or with an IDE
In the case of building and running with the command line execute the following Maven commands in the root directory of the project:
```sh
mvn clean install
mvn exec:java
```
Or build & run from an IDE
The client should now be accessible at [localhost:8080](localhost:8080) 

**Build and run frontend**
Install the dependencies and devDependencies in the directory called **client** in the root directory using the 'npm install command'. Build and run the development server using the 'npm run serve' command.
```sh
$ npm install
$ npm run serve
```
The client should now be accessible at [localhost:8081](localhost:8081) 

### Runtime testing
The application can be runtime-tested using the user interface at [localhost:8081](localhost:8081). The Vue client and Spring Boot application should be running in that case. An user registration can be performed by filling  out the user registration form. A database browser (e.g.PGadmin) could be used to check if the registered user  has been inserted properly into the database. A user login can be performed with am user present in the database.

### Testing with Cypress
Other than with the unittests, Cypress tests do not get executed while building the application. Therefore, on your local machine, start the frontend with the following command:
```sh
$ npm run serve
```
Then, open a second command prompt and execute the follwing command:
```sh
$ npm run cypress:open
```
A cypress GUI will open up and the desired integration and end-to-end tests can be selected and executed

## Run with Docker
---
It is possible to test the application in a different environment with Docker. For prerequisites and steps to follow, see below.

### Prerequisites
The following things are required to test the application with Docker:
  - Docker or Docker Toolbox(on Windows Home)
  - Docker Compose
  - Use **192.168.99.100** as Host IP address when using Docker Toolbox (which is the default IP address in Docker Toolbox)
    *Please note: if you are using the regular Docker or another Host IP address on Docker toolbox than the one above, you will have to change the Host address in the **VUE_APP_API_BASE_URL** variable in the **.env.production** file in **client folder** as well.*
  - Ensure port **8080** and port **8081** are accessible on the host

### Runtime testing
In the *Docker Quickstart Terminal* make sure you are in the root of the Harvest Insurances application. There, run the Docker Compose file:
```sh
$ docker-compose up
```
Docker will now build and run the PostgreSQL database,Spring Boot Rest API and VUE CLI frontend. 

Once All the Docker containers are running, you can access the frontend at **192.168.99.100:8081** and start runtime testing the application, as described in the section *Runtime testing* under *Local Environment*.
 
