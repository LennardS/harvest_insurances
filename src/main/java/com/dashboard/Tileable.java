package com.dashboard;

import java.util.Map;

public interface Tileable {
    enum TileType {
        USER_LOGIN_TILE,
        USER_PROFILE_TILE
    }

    Tileable createDashboardTile();
    Map<String, String> getTileData();
    TileType getTileType();
}
