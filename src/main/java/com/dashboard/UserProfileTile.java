package com.dashboard;

import com.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class UserProfileTile extends UserLoginTile implements Tileable {
    @Getter
    private Map<String, String> userProfileData = new HashMap<>();
    private TileType tileType = Tileable.TileType.USER_PROFILE_TILE;

    public UserProfileTile(User user) {
        super(user);
    }

    @Override
    public Tileable createDashboardTile() {
        if(rootUser == null)
            throw new NullPointerException("cannot create UserProfileTile -> this.user is null");

        userProfileData.put("bsn", rootUser.getBsn().toString());
        userProfileData.put("fullName", rootUser.getFullName());
        userProfileData.put("address", rootUser.getAddress());
        userProfileData.put("country", rootUser.getCountry());
        userProfileData.put("zipCode", rootUser.getZipCode());

        return this;
    }

    @Override
    public Map<String, String> getTileData() {
        return userProfileData;
    }

    @Override
    public TileType getTileType() {
        return this.tileType;
    }
}
