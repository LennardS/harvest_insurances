package com.dashboard;

import com.user.User;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.HashMap;
import java.util.Map;

@NoArgsConstructor
public class UserLoginTile implements Tileable {
    @Getter
    private Map<String, String> userLoginData = new HashMap<>();
    protected User rootUser;
    protected TileType tileType = Tileable.TileType.USER_LOGIN_TILE;

    public UserLoginTile(User user) {
        this.rootUser = user;
    }


    @Override
    public Tileable createDashboardTile() throws NullPointerException {
        if(rootUser == null)
            throw new NullPointerException("cannot create UserLoginTile -> this.user is null");

        userLoginData.put("bsn", rootUser.getBsn().toString());
        userLoginData.put("emailAddress", rootUser.getEmailAddress());
        userLoginData.put("password", "");

        return this;
    }

    @Override
    public Map<String, String> getTileData() {
        return userLoginData;
    }

    @Override
    public TileType getTileType() {
        return this.tileType;
    }
}
