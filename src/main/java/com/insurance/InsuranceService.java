package com.insurance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.insurance.InsuranceOptionDAO;
import com.insurance.InsuranceDAO;

@Service
public class InsuranceService {
    private InsuranceDAO insuranceRepo;
    private InsuranceOptionDAO insuranceOptionRepo;

    @Autowired
    public InsuranceService(InsuranceDAO insuranceRepo, InsuranceOptionDAO insuranceOptionRepo) {
        this.insuranceRepo = insuranceRepo;
        this.insuranceOptionRepo = insuranceOptionRepo;
    }
}
