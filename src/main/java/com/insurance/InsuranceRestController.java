package com.insurance;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import com.insurance.InsuranceService;

@RestController
public class InsuranceRestController {
    private InsuranceService insuranceService;


    @Autowired
    public InsuranceRestController(InsuranceService insuranceService) {
        this.insuranceService = insuranceService;
    }
}
