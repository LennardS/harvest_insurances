package com.insurance;

import javax.persistence.*;
import java.util.Set;

@Entity
@Table(name="insurance")
public class Insurance {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="insurance_id")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="cost")
    private double cost;

    @Column(name="insurance_option")
    @JoinColumn(name = "insurance_id")
    @OneToMany(cascade = CascadeType.ALL)
    private Set<InsuranceOption> insuranceOptions;
}
