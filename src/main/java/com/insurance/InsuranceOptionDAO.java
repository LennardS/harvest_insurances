package com.insurance;

import com.insurance.InsuranceOption;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuranceOptionDAO extends CrudRepository<InsuranceOption, Long> {
}
