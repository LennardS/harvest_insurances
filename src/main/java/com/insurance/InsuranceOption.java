package com.insurance;

import javax.persistence.*;

@Entity
@Table(name="insurance_option")
public class InsuranceOption {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name="insturance_option_id")
    private Long id;

    @Column(name="name")
    private String name;

    @Column(name="description")
    private String description;

    @Column(name="cost")
    private double cost;
}
