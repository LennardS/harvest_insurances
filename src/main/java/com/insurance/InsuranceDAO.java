package com.insurance;

import com.insurance.Insurance;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InsuranceDAO extends CrudRepository<Insurance, Long>{

}