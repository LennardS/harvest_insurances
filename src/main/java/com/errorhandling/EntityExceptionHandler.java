package com.errorhandling;

import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Set;

    @RestControllerAdvice
    public class EntityExceptionHandler extends ResponseEntityExceptionHandler {

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        Set<String> errorMessages = new HashSet<>();
        for (ObjectError m : ex.getBindingResult().getAllErrors()) {
            errorMessages.add(m.getDefaultMessage());
        }
        ErrorModel error = new ErrorModel(HttpStatus.BAD_REQUEST, "Validatie error", errorMessages);

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    private ResponseEntity<ErrorModel> handleEntityNotFound(EntityNotFoundException ex){
        ErrorModel error = new ErrorModel(HttpStatus.NOT_FOUND, "Entiteit niet gevonden", ex.getMessage());

        return new ResponseEntity<>(error, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = { TransactionSystemException.class, ConstraintViolationException.class })
    private ResponseEntity<ErrorModel> handleDbValidationException(TransactionSystemException ex) {
        Throwable cause = ex.getRootCause();
        Set<String> errorMessages = new HashSet<>();
        ErrorModel error;

        if (cause instanceof ConstraintViolationException) {
            Set<ConstraintViolation<?>> constraintViolations = ((ConstraintViolationException) cause).getConstraintViolations();

            for(ConstraintViolation violation : constraintViolations) {
                errorMessages.add(violation.getMessage());
            }
            error = new ErrorModel(HttpStatus.BAD_REQUEST, "Validatie error", errorMessages);
        } else {
            errorMessages.add(ex.getMessage());
            error = new ErrorModel(HttpStatus.BAD_REQUEST, "Database transactie error", errorMessages);
        }
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = DataIntegrityViolationException.class)
    private ResponseEntity<ErrorModel> handleDataIntigrityViolationException(DataIntegrityViolationException ex) {
        Set<String> errormessages = new HashSet<>();
        ErrorModel error;

        errormessages.add(ex.getMessage());
        error = new ErrorModel(HttpStatus.BAD_REQUEST, "Validatie error", errormessages);

        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
