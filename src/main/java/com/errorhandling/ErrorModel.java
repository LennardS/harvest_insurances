package com.errorhandling;

import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;
import java.util.Set;

public class ErrorModel {
    private HttpStatus httpStatus;
    private LocalDateTime timestamp;
    private String message;
    private String details ="More detailed description is in the message below";
    private Set<String> errorMessages;

    public ErrorModel(HttpStatus httpStatus, String message, Set<String> errorMessages) {
        this.httpStatus = httpStatus;
        this.timestamp = LocalDateTime.now();
        this.message = message;
        this.errorMessages = errorMessages;
    }

    public ErrorModel(HttpStatus httpStatus, String message, String details) {
        this.httpStatus = httpStatus;
        this.timestamp = LocalDateTime.now();
        this.message = message;
        this.details = details;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public String getDetails() {
         return details;
    }

    public Set<String> getErrorMessages() {
        return errorMessages;
    }
}
