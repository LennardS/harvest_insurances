package com.user;

import com.dashboard.Tileable;
import com.dashboard.UserLoginTile;
import com.dashboard.UserProfileTile;
import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionSystemException;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.persistence.EntityNotFoundException;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintViolationException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

@Service
public class UserService implements UserDetailsService {
    private UserDAO userDAO;
    private BlackListTokenDAO blackListTokenDAO;

    @Getter
    @Setter
    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Autowired
    public UserService(UserDAO userRepo, BlackListTokenDAO blackListTokenDAO) {
        this.userDAO = userRepo;
        this.blackListTokenDAO = blackListTokenDAO;
    }

    @Override
    public UserDetails loadUserByUsername(String bsn) throws UsernameNotFoundException {
        if (userDAO.findById(Long.parseLong(bsn)).isPresent()) {
            User user = userDAO.findById(Long.parseLong(bsn)).get();
            return new org.springframework.security.core.userdetails.User(
                    user.getBsn().toString(), user.getPassword(),
                    new ArrayList<>());
        } else {
            throw new UsernameNotFoundException("User not found with username: " + bsn);
        }
    }

    public User findUserById(Long bsn) throws EntityNotFoundException {
        if(userDAO.findById(bsn).isPresent()) {
            return userDAO.findById(bsn).get();
        } else {
            throw new EntityNotFoundException();
        }
    }

    public User createAndSaveUser(UserDTO userDto, HttpServletResponse httpResponse) {
        User user = new User(userDto);
        user.setPassword(bcryptEncoder.encode(user.getPassword()));

        return saveUserToDb(user, httpResponse);
    }

    public void deleteUser(User user, HttpServletResponse httpResponse) {
        if(!userDAO.findById(user.getBsn()).isPresent()) {
            throw new DataIntegrityViolationException("Deze gebruiker bestaat niet en kan dus niet verwijderd worden");
        }
        userDAO.delete(user);
        httpResponse.setStatus(HttpServletResponse.SC_OK);
    }

    private User saveUserToDb(User user, HttpServletResponse httpResponse) throws TransactionSystemException, DataIntegrityViolationException {
        if(userDAO.findById(user.getBsn()).isPresent()) {
            throw new DataIntegrityViolationException("Deze gebruiker bestaat al. Log in of maak een gebruiker met een ander bsn-nummer aan");
        }
        user = userDAO.save(user);
        httpResponse.setStatus(HttpServletResponse.SC_CREATED);
        return user;
    }

    public BlackListToken findTokenById(String token) {
        Iterator it = blackListTokenDAO.findAll().iterator();
        while(it.hasNext()) {
            BlackListToken blackListToken = (BlackListToken)it.next();
            if(blackListToken.getTokenStr().equals(token))
                return blackListToken;
        }
        return null;
    }

    public BlackListToken saveBlackListToken(BlackListToken token) throws ConstraintViolationException {
        if(findTokenById(token.getTokenStr()) != null) {
            return token;
        } else if(token.equals("")) {
            return null;
        } else {
            return blackListTokenDAO.save(token);
        }
    }

    public Set<Tileable> getAllDashboardTiles(Long bsn) {
        User user = this.findUserById(bsn);
        Tileable loginTile = new UserLoginTile(user);
        Tileable profileTile = new UserProfileTile(user);

        loginTile = loginTile.createDashboardTile();
        profileTile = profileTile.createDashboardTile();

        user.addToDashBoardTiles(loginTile);
        user.addToDashBoardTiles(profileTile);

        return user.getDashBoardTiles();
    }
}
