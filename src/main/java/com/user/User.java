package com.user;

import com.dashboard.Tileable;
import com.dashboard.UserProfileTile;
import com.validation.annotations.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="userdata")
public class User extends UserProfileTile {
    @Id
    @BsnLongConstraint
    private Long bsn;

    @FullNameConstraint
    private String fullName;

    @AddressConstraint
    private String address;

    @CountryConstraint
    private String country;

    @ZipCodeConstraint
    private String zipCode;

    @EmailConstraint
    private String emailAddress;

    //the password is already hashed at this point, so no validation anymore when save to db
    private String password;

    @Transient
    private Set<Tileable> dashBoardTiles = new HashSet<>();

    public User(UserDTO userDto) throws NumberFormatException {
        this.bsn = Long.valueOf(userDto.getBsn());
        this.fullName = userDto.getFullName();
        this.emailAddress = userDto.getEmailAddress();
        this.country = userDto.getCountry();
        this.zipCode = userDto.getZipCode();
        this.address = userDto.getAddress();
        this.password = userDto.getPassword();
    }

    public void addToDashBoardTiles(Tileable tile) {
        this.dashBoardTiles.add(tile);
    }
}
