package com.user;

import com.validation.annotations.BlackListTokenConstraint;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@NoArgsConstructor
@Getter
@Setter
@Entity
@Table(name="blacklist_token")
public class BlackListToken {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    @BlackListTokenConstraint
    private String tokenStr;
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date blackListDate = new Date(System.currentTimeMillis());
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME)
    private Date tokenExpiryDate;

    public BlackListToken(String tokenStr, Date tokenExpiryDate) {
        this.tokenStr = tokenStr;
        this.tokenExpiryDate = tokenExpiryDate;
    }
}
