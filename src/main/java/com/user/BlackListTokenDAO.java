package com.user;

import org.springframework.data.repository.CrudRepository;

public interface BlackListTokenDAO extends CrudRepository<BlackListToken, Long> {
}
