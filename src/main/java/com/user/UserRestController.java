package com.user;

import com.dashboard.Tileable;
import com.websecurityconfig.JwtTokenUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Set;

@RestController
@EnableWebMvc
public class UserRestController {
    private UserService userService;
    private AuthenticationManager authenticationManager;
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    public UserRestController(UserService userService, AuthenticationManager authenticationManager, JwtTokenUtil jwtTokenUtil) {
        this.userService = userService;
        this.authenticationManager = authenticationManager;
        this.jwtTokenUtil = jwtTokenUtil;
    }

    @PostMapping("/user")
    public UserDTO createUser(@RequestBody @Valid UserDTO userDto, HttpServletResponse httpResponse) {
        User user = userService.createAndSaveUser(userDto, httpResponse);
        UserDTO userDTO = new UserDTO(user);
        return userDto;
    }

    @PostMapping("/user/session")
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationModel authenticationModel) throws Exception {
        authenticate(authenticationModel.getUserName(), authenticationModel.getPassword());
        final UserDetails userDetails = userService
                .loadUserByUsername(authenticationModel.getUserName());
        final String token = jwtTokenUtil.generateToken(userDetails);
        return ResponseEntity.ok(new JwtResponse(token));
    }

    @PostMapping("/user/session/invalidate")
    public ResponseEntity<?> invalidateAuthenticationToken(HttpServletRequest request) {
        String jwtToken = request.getHeader("Authorization");
        jwtToken = jwtTokenUtil.trimToUsableToken(jwtToken);
        if(!jwtToken.equals(""))
            userService.saveBlackListToken(new BlackListToken(jwtToken, jwtTokenUtil.getExpirationDateFromToken(jwtToken)));

        return ResponseEntity.ok().build();
    }

    @GetMapping("/csrfToken")
    public ResponseEntity<?> createCsrfToken() {
        return ResponseEntity.ok().build();
    }

    @GetMapping("/user/tiles")
    public ResponseEntity<?> getDashboardTiles(HttpServletRequest request) {
        String jwtToken = request.getHeader("Authorization");
        jwtToken = jwtTokenUtil.trimToUsableToken(jwtToken);
        Long userBsn = Long.parseLong(jwtTokenUtil.getUsernameFromToken(jwtToken));
        Set<Tileable> dashBoardTiles = userService.getAllDashboardTiles(userBsn);

        return new ResponseEntity<>(dashBoardTiles, HttpStatus.OK);
    }

    public void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }
}
