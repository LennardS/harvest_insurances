package com.user;

import com.validation.annotations.*;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class UserDTO {
    @BsnStringConstraint
    private String bsn;
    @FullNameConstraint
    private String fullName;
    @AddressConstraint
    private String address;
    @EmailConstraint
    private String emailAddress;
    @CountryConstraint
    private String country;
    @ZipCodeConstraint
    private String zipCode;
    @PasswordConstraint
    private String password;

    public UserDTO(User user) {
        this.bsn = user.getBsn().toString();
        this.fullName = user.getFullName();
        this.address = user.getAddress();
        this.zipCode = user.getZipCode();
        this.emailAddress = user.getEmailAddress();
        this.country = user.getCountry();
        this.password = user.getPassword();
    }

    public UserDTO(String bsn, String fullName, String address, String emailAddress, String country, String zipCode, String password) {
        this.bsn = bsn;
        this.fullName = fullName;
        this.address = address;
        this.emailAddress = emailAddress;
        this.zipCode = zipCode;
        this.country = country;
        this.password = password;
    }
}
