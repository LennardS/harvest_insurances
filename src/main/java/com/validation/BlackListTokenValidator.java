package com.validation;

import com.validation.annotations.BlackListTokenConstraint;
import lombok.Getter;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@Configuration
public class BlackListTokenValidator implements ConstraintValidator<BlackListTokenConstraint, String> {
    @Getter
    private String blackListTokenPattern;

    @Override
    public void initialize(BlackListTokenConstraint constraintAnnotation) {
        blackListTokenPattern = "^[a-zA-Z0-9._-]{100,}$";
    }

    @Override
    public boolean isValid(String blackListToken, ConstraintValidatorContext constraintValidatorContext) {
        return blackListToken.matches(blackListTokenPattern);
    }
}
