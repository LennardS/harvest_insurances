package com.validation;

import com.validation.annotations.BsnStringConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class BsnStringValidator implements ConstraintValidator<BsnStringConstraint, String> {
    @Getter
    private String bsnPattern;

    @Override
    public void initialize(BsnStringConstraint constraintAnnotation) {
        bsnPattern = "^[0-9]{9}$";
    }

    @Override
    public boolean isValid(String bsn, ConstraintValidatorContext constraintValidatorContext) {
        if(bsn.matches(bsnPattern)) {
            return true;
        }
        return false;
    }
}
