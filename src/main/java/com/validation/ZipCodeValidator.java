package com.validation;

import com.validation.annotations.ZipCodeConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class ZipCodeValidator implements ConstraintValidator<ZipCodeConstraint, String> {
    @Getter
    private String zipCodePattern;


    @Override
    public void initialize(ZipCodeConstraint constraintAnnotation) {
        zipCodePattern = "^[1-9][0-9]{3} ?(?!sa|sd|ss)[A-Z]{2}$";
    }

    @Override
    public boolean isValid(String fullName, ConstraintValidatorContext constraintValidatorContext) {
        return fullName.matches(zipCodePattern);
    }
}
