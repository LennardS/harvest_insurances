package com.validation;

import com.validation.annotations.CountryConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CountryValidator implements ConstraintValidator<CountryConstraint, String> {
    @Getter
    private String countryPattern;

    @Override
    public void initialize(CountryConstraint constraintAnnotation) {
        countryPattern = "^[a-zA-Z\u0080-\u024F\\s\\/\\-\\)\\(\\`\\.\"\']{2,}$";
    }

    @Override
    public boolean isValid(String fullName, ConstraintValidatorContext constraintValidatorContext) {
        return fullName.matches(countryPattern);
    }
}