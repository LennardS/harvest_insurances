package com.validation;

import com.validation.annotations.AddressConstraint;
import lombok.Getter;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
@Configuration
public class AddressValidator implements ConstraintValidator<AddressConstraint, String> {
    @Getter
    private String addressPattern;

    @Override
    public void initialize(AddressConstraint constraintAnnotation) {
        addressPattern = "^[A-Za-z ]+ [0-9]+[A-Za-z]?$";
    }

    @Override
    public boolean isValid(String fullName, ConstraintValidatorContext constraintValidatorContext) {
        return fullName.matches(addressPattern);
    }

}
