package com.validation;

import com.validation.annotations.BsnLongConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class BsnLongValidator implements ConstraintValidator<BsnLongConstraint, Long> {
    @Getter
    private String bsnPattern;

    @Override
    public void initialize(BsnLongConstraint constraintAnnotation) {
        bsnPattern = "^[0-9]{9}$";
    }

    @Override
    public boolean isValid(Long bsn, ConstraintValidatorContext constraintValidatorContext) {
        if(bsn.toString().matches(bsnPattern)) {
            return true;
        }
        return false;
    }
}
