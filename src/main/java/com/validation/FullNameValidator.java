package com.validation;

import com.validation.annotations.FullNameConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class FullNameValidator implements ConstraintValidator<FullNameConstraint, String> {
    @Getter
    private String fullNamePattern;

    @Override
    public void initialize(FullNameConstraint constraintAnnotation) {
        fullNamePattern = "^([A-Za-z]+ [A-Za-z]+)+$";
    }

    @Override
    public boolean isValid(String fullName, ConstraintValidatorContext constraintValidatorContext) {
        return fullName.matches(fullNamePattern);
    }
}
