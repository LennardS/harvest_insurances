package com.validation;

import com.validation.annotations.EmailConstraint;
import lombok.Getter;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class EmailValidator implements ConstraintValidator<EmailConstraint, String> {
    @Getter
    private String emailPattern;

    @Override
    public void initialize(EmailConstraint constraintAnnotation) {
        emailPattern = "^[A-Za-z0-9+_.-]+@[A-Za-z0-9+_.-]+\\.[a-z]{2,3}$";
    }

    @Override
    public boolean isValid(String fullName, ConstraintValidatorContext constraintValidatorContext) {
        return fullName.matches(emailPattern);
    }
}
