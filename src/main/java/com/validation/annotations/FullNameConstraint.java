package com.validation.annotations;

import com.validation.FullNameValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { FullNameValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface FullNameConstraint {
        String message() default "Vul een juiste 'volledige naam' in";

        Class<?>[] groups() default {};

        Class<? extends Payload>[] payload() default {};
}
