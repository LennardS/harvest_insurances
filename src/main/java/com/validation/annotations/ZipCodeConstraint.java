package com.validation.annotations;

import com.validation.ZipCodeValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { ZipCodeValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface ZipCodeConstraint {
    String message() default "De ingevoerde postcode is niet juist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
