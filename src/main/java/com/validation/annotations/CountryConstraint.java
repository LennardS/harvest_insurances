package com.validation.annotations;

import com.validation.CountryValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { CountryValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface CountryConstraint {
    String message() default "Het ingevoerde land is niet juist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}