package com.validation.annotations;

import com.validation.BlackListTokenValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { BlackListTokenValidator.class })
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface BlackListTokenConstraint {
    String message() default "The authentication token is not in the right format";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
