package com.validation.annotations;

import com.validation.EmailValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { EmailValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface EmailConstraint {
    String message() default "Vul een geldig emailadres  in";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
