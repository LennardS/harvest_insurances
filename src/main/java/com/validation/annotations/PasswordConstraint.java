package com.validation.annotations;

import com.validation.PasswordValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { PasswordValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface PasswordConstraint {
    String message() default "Het wachtwoord moet bestaan uit tenminste 8 karakters, waarvan minstens 1 cijfer, 1 hoofdletter en 1 teken";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
