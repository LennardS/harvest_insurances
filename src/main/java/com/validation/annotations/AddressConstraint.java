package com.validation.annotations;

import com.validation.AddressValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = { AddressValidator.class })
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface AddressConstraint {
    String message() default "Het ingevoerde adres is niet juist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
