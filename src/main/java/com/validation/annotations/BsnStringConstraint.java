package com.validation.annotations;

import com.validation.BsnStringValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = {BsnStringValidator.class})
@Target({ ElementType.METHOD, ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface BsnStringConstraint {
    String message() default "Het ingevoerde bsn-nummer is niet juist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
