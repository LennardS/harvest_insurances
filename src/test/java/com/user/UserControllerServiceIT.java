package com.user;

import com.errorhandling.EntityExceptionHandler;
import com.helper.TestDataUserDTO;
import com.helper.TestDataUserDTOImpl;
import com.websecurityconfig.JwtTokenUtil;
import io.jsonwebtoken.SignatureException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.util.NestedServletException;

import java.util.*;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = {UserRestController.class, UserService.class, UserDAO.class, EntityExceptionHandler.class})
@AutoConfigureMockMvc
@EntityScan("com")
@ComponentScan("com")
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class UserControllerServiceIT {
    private TestDataUserDTO testDataUserDTO;
    private UserRestController restController;
    @Mock
    private UserDAO userDAO;
    @Mock
    private BlackListTokenDAO blackListTokenDAO;
    @Mock
    private BCryptPasswordEncoder encoder;
    @InjectMocks
    private UserService userService = new UserService(userDAO, blackListTokenDAO);
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private MockMvc mvc;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void setUp() {
        restController = new UserRestController(userService, authenticationManager, jwtTokenUtil);

        userService.setBcryptEncoder(encoder);
        testDataUserDTO = new TestDataUserDTOImpl();
    }

    @Test
    public void getCsrfToken() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/csrfToken")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andReturn();
        assertNotNull(mvcResult.getResponse().getCookie("XSRF-TOKEN"));
    }

    @Test
    public void createUser() throws Exception {
        UserDTO correctUserDto = testDataUserDTO.getCorrectUserDTO();
        User correctUser = new User(correctUserDto);

        when(userDAO.findById(correctUser.getBsn())).thenReturn(Optional.empty());
        when(userDAO.save(any(User.class))).thenReturn(correctUser);
        mvc = MockMvcBuilders.standaloneSetup(restController).build();
        String jsonStr = "{\"bsn\":\"" + correctUser.getBsn() + "\",\"fullName\":\"" + correctUser.getFullName() + "\",\"address\":\""+
                correctUser.getAddress() + "\",\"zipCode\":\""+ correctUser.getZipCode() +"\",\"emailAddress\": \""+
                correctUser.getEmailAddress() +"\" ,\"country\": \""+ correctUser.getCountry() +"\" ,\"password\": \""+ correctUser.getPassword() +"\"}";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isCreated())
                .andExpect(content().json(jsonStr)).andReturn();
    }

    @Test
    public void createFailedUser() throws Exception {
        UserDTO failedUserDto = testDataUserDTO.getFailedUserDTO();
        User failedUser = new User(failedUserDto);
        String jsonStr = "{\"bsn\":\"" + failedUser.getBsn() + "\",\"fullName\":\"" + failedUser.getFullName() + "\",\"address\":\""+
                failedUser.getAddress() + "\",\"zipCode\":\""+ failedUser.getZipCode() +"\",\"emailAddress\": \""+
                failedUser.getEmailAddress() +"\" ,\"country\": \""+ failedUser.getCountry() +"\" ,\"password\": \""+ failedUser.getPassword() +"\"}";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest()).andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains("Validatie error"));
    }

    @Test
    public void createPartiallyFailedUser() throws Exception {
        UserDTO partiallyFailedUserDTO = testDataUserDTO.getPartiallyFailedUserDTO();
        User partiallyFailedUser = new User(partiallyFailedUserDTO);
        String jsonStr = "{\"bsn\":\"" + partiallyFailedUser.getBsn() + "\",\"fullName\":\"" + partiallyFailedUser.getFullName() + "\",\"address\":\""+
                partiallyFailedUser.getAddress() + "\",\"zipCode\":\""+ partiallyFailedUser.getZipCode() +"\",\"emailAddress\": \""+
                partiallyFailedUser.getEmailAddress() +"\" ,\"country\": \""+ partiallyFailedUser.getCountry() +"\" ,\"password\": \""+ partiallyFailedUser.getPassword() +"\"}";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isBadRequest()).andReturn();

        assertTrue(mvcResult.getResponse().getContentAsString().contains("Validatie error"));
    }

    @Test
    public void noCsrfToken() throws Exception {
        UserDTO correctUserDto = testDataUserDTO.getPartiallyFailedUserDTO();
        User correctUser = new User(correctUserDto);

        String jsonStr = "{\"bsn\":\"" + correctUser.getBsn() + "\",\"fullName\":\"" + correctUser.getFullName() + "\",\"address\":\""+
                correctUser.getAddress() + "\",\"zipCode\":\""+ correctUser.getZipCode() +"\",\"emailAddress\": \""+
                correctUser.getEmailAddress() +"\" ,\"country\": \""+ correctUser.getCountry() +"\" ,\"password\": \""+ correctUser.getPassword() +"\"}";

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user").contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isForbidden()).andReturn();

        assertTrue(mvcResult.getResponse().getHeader("Set-Cookie").contains("XSRF-TOKEN"));
    }

    @Test
    public void authenticateUserSuccessfully() throws Exception {
        performSuccessfulAuthentication();
    }

    @Test
    public void authenticateUserDoesNotExist() throws Exception {
        restController = Mockito.spy(restController);
        UserDTO correctUserDto = testDataUserDTO.getPartiallyFailedUserDTO();

        Mockito.doNothing().when(restController).authenticate(correctUserDto.getBsn(), correctUserDto.getPassword());
        mvc = MockMvcBuilders.standaloneSetup(restController).build();
        String jsonStr = "{\"userName\": \""+ correctUserDto.getBsn() +"\" ,\"password\": \""+ correctUserDto.getPassword() +"\"}";

        //should be caused by userNameNotFoundException
        exception.expect(NestedServletException.class);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user/session").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isUnauthorized()).andReturn();
    }

    @Test
    public void badUserInputAuthentication() throws Exception {
        restController = Mockito.spy(restController);
        UserDTO emptyUserDto = testDataUserDTO.getPartiallyFailedUserDTO();

        Mockito.doNothing().when(restController).authenticate(emptyUserDto.getBsn(), emptyUserDto.getPassword());
        mvc = MockMvcBuilders.standaloneSetup(restController).build();
        String jsonStr = "{\"userName\": \""+ emptyUserDto.getBsn() +"\" ,\"password\": \""+ emptyUserDto.getPassword() +"\"}";

        //should be caused by userNameNotFoundException
        exception.expect(NestedServletException.class);
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user/session").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(jsonStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isUnauthorized()).andReturn();
    }

    @Test
    public void invalidateSession() throws Exception {
        HttpHeaders httpHeaders = this.constructAuthenticationHeaders(new HttpHeaders());

        mvc.perform(MockMvcRequestBuilders
                .post("/user/session/invalidate").with(csrf())
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void invalidateEmptySession() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenStr = "";
        httpHeaders.add("Authorization", tokenStr);

        mvc.perform(MockMvcRequestBuilders
                .post("/user/session/invalidate").with(csrf())
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void invalidateWrongSession() throws Exception {
        HttpHeaders httpHeaders = new HttpHeaders();
        String tokenStr = "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c";
        httpHeaders.add("Authorization", tokenStr);

        exception.expect(SignatureException.class);
       mvc.perform(MockMvcRequestBuilders
                .post("/user/session/invalidate").with(csrf())
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void invalidateDuplicateSession() throws Exception {
        HttpHeaders httpHeaders = constructAuthenticationHeaders(new HttpHeaders());

        mvc.perform(MockMvcRequestBuilders
                .post("/user/session/invalidate").with(csrf())
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();

        mvc.perform(MockMvcRequestBuilders
                .post("/user/session/invalidate").with(csrf())
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();
    }

    @Test
    public void getUserTiles() throws Exception {
        HttpHeaders httpHeaders = this.constructAuthenticationHeaders(new HttpHeaders());

        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders
                .get("/user/tiles")
                .headers(httpHeaders)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();

        String jsonBody = mvcResult.getResponse().getContentAsString();
        String[] bodyArrayBsn = jsonBody.split("bsn");
        String[] bodyArrayZipCode = jsonBody.split("zipCode");
        String[] bodyArrayEmailAddress = jsonBody.split("emailAddress");

        assertEquals(5, bodyArrayBsn.length);
        assertEquals(3, bodyArrayZipCode.length);
        assertEquals(3, bodyArrayEmailAddress.length);
    }

    @Test
    public void getUserTilesAccessDenied() throws Exception {
        mvc.perform(MockMvcRequestBuilders
                .get("/user/tiles")
                .characterEncoding("utf-8"))
                .andExpect(status().isUnauthorized()).andReturn();
    }

    private MvcResult performSuccessfulAuthentication() throws Exception {
        UserDTO correctUserDTO = testDataUserDTO.getCorrectUserDTO();
        restController = spy(restController);
        userService = spy(userService);
        mvc = MockMvcBuilders.standaloneSetup(restController).build();

        User user = new User(testDataUserDTO.getCorrectUserDTO());
        Mockito.doNothing().when(restController).authenticate(correctUserDTO.getBsn(), correctUserDTO.getPassword());
        when(userDAO.findById(user.getBsn())).thenReturn(Optional.of(user));

        String loginStr = "{\"userName\": \""+ user.getBsn() +"\" ,\"password\": \""+ user.getPassword() +"\"}";
        MvcResult loginMvcResult = mvc.perform(MockMvcRequestBuilders
                .post("/user/session").with(csrf()).contentType(MediaType.APPLICATION_JSON)
                .content(loginStr)
                .characterEncoding("utf-8"))
                .andExpect(status().isOk()).andReturn();

        verify(restController, times(1)).authenticate(user.getBsn().toString(), user.getPassword());
        return loginMvcResult;
    }

    private HttpHeaders constructAuthenticationHeaders(HttpHeaders headers) throws Exception {
        String token = this.performSuccessfulAuthentication().getResponse().getContentAsString().substring(10);
        token = token.substring(0, token.length()-2);
        String tokenStr = "Bearer " + token;
        headers.add("Authorization", tokenStr);

        return headers;
    }

    
}
