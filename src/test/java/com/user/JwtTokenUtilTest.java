package com.user;

import com.websecurityconfig.JwtTokenUtil;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class JwtTokenUtilTest {
    private String token = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg";
    @Mock
    private UserDetails userDetails;
    @Mock
    private UserService userService;
    @Mock
    private BlackListToken blackListToken;
    @Mock
    private JwtTokenUtil jwtTokenUtil = new JwtTokenUtil();

    @Before
    public void init() {
    }

    @Test
    public void tokenIsExpired() {
        when(userDetails.getUsername()).thenReturn("TestUser");
        when(userService.findTokenById(token)).thenReturn(blackListToken);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn("TestUser");
        when(jwtTokenUtil.validateToken(token, userDetails, userService)).thenCallRealMethod();

        boolean result = jwtTokenUtil.validateToken(token, userDetails, userService);

        assertFalse(result);
    }

    @Test
    public void tokenIsNotExpired() {
        when(userDetails.getUsername()).thenReturn("TestUser");
        when(userService.findTokenById(token)).thenReturn(null);
        when(jwtTokenUtil.getUsernameFromToken(token)).thenReturn("TestUser");
        when(jwtTokenUtil.getExpirationDateFromToken(token)).thenReturn(new Date(System.currentTimeMillis()+14400));
        when(jwtTokenUtil.validateToken(token, userDetails, userService)).thenCallRealMethod();

        boolean result = jwtTokenUtil.validateToken(token, userDetails, userService);

        assertTrue(result);
    }
}
