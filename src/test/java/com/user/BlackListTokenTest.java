package com.user;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
public class BlackListTokenTest {
    @Mock
    private UserDAO userDAO;
    @Mock
    private BlackListTokenDAO tokenDAO;

    @InjectMocks
    private UserService userService = new UserService(userDAO, tokenDAO);

    @Before
    public void init() {
    }

    @Test
    public void addTokenToBlackList() {
        userService = Mockito.spy(userService);
        BlackListToken token = new BlackListToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg",
                new Date(System.currentTimeMillis() + 14400 * 1000)
                );

        when(tokenDAO.findAll()).thenReturn(new ArrayList<>());
        when(tokenDAO.save(any(BlackListToken.class))).thenReturn(token);

        BlackListToken result = userService.saveBlackListToken(token);
        verify(tokenDAO, times(1)).save(any(BlackListToken.class));
        assertEquals(token, result);
    }

    @Test
    public void addDuplicateTokenToBlackList() {
        userService = Mockito.spy(userService);
        BlackListToken token = new BlackListToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg",
                new Date(System.currentTimeMillis() + 14400 * 1000)
        );
        List<BlackListToken> tokenList = new ArrayList<>();
        tokenList.add(token);
        when(tokenDAO.save(any(BlackListToken.class))).thenReturn(token);

        BlackListToken initialToken = userService.saveBlackListToken(token);
        assertEquals(token, initialToken);

        when(tokenDAO.findAll()).thenReturn(tokenList);

        BlackListToken duplicatToken = userService.saveBlackListToken(token);
        verify(userService, times(2)).findTokenById(token.getTokenStr());
        verify(tokenDAO, times(1)).save(any(BlackListToken.class));
        assertEquals(token, duplicatToken);
    }
}
