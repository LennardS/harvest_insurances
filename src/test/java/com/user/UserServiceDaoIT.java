package com.user;

import com.dashboard.Tileable;
import com.dashboard.UserLoginTile;
import com.dashboard.UserProfileTile;
import com.helper.TestDataUserDTO;
import com.helper.TestDataUserDTOImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionSystemException;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.Set;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { UserService.class, UserDAO.class})
@EnableAutoConfiguration
@EntityScan("com")
@ComponentScan("com")
@ActiveProfiles("test")
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class UserServiceDaoIT {
    @Autowired
    private BlackListTokenDAO blackListTokenDAO;
    @Autowired
    private UserDAO userDAO;
    @Autowired
    private PasswordEncoder bcryptEncoder;
    private UserService userService;
    private TestDataUserDTO testDataUserDTO;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        userService = new UserService(userDAO, blackListTokenDAO);
        userService.setBcryptEncoder(bcryptEncoder);
        testDataUserDTO = new TestDataUserDTOImpl();
    }

    @Test
    public void createSaveDeleteUserCorrectly() {
        UserDTO userDto = testDataUserDTO.getCorrectUserDTO();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        User savedUser = userService.createAndSaveUser(userDto, mockHttpServletResponse);
        UserDetails loadedUser = userService.loadUserByUsername(userDto.getBsn());

        assertEquals(9, loadedUser.getUsername().length());
        assertEquals(60, loadedUser.getPassword().length());
        assertNotEquals(testDataUserDTO.getCorrectUserDTO(), loadedUser.getPassword());

        userService.deleteUser(savedUser, mockHttpServletResponse);

        exception.expect(UsernameNotFoundException.class);
        userService.loadUserByUsername(userDto.getBsn());
    }

    @Test
    public void createFailedUser() {
        UserDTO userDto = testDataUserDTO.getFailedUserDTO();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        exception.expect(TransactionSystemException.class);
        userService.createAndSaveUser(userDto, mockHttpServletResponse);
    }

    @Test
    public void createPartiallyFailedUser() {
        UserDTO userDto = testDataUserDTO.getPartiallyFailedUserDTO();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        exception.expect(TransactionSystemException.class);
        userService.createAndSaveUser(userDto, mockHttpServletResponse);
    }

    @Test
    public void deleteNonExistingUser() {
        UserDTO userDto = testDataUserDTO.getCorrectUserDTO();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        exception.expect(DataIntegrityViolationException.class);
        userService.deleteUser(new User(userDto), mockHttpServletResponse);
    }

    @Test
    public void createDuplicateUser() {
        UserDTO userDTO = testDataUserDTO.getCorrectUserDTO();
        MockHttpServletResponse mockHttpServletResponse = new MockHttpServletResponse();

        User savedUser = userService.createAndSaveUser(userDTO, mockHttpServletResponse);

        assertNotEquals(userDTO.getPassword(), savedUser.getPassword());
        assertEquals(60, savedUser.getPassword().length());

        exception.expect(DataIntegrityViolationException.class);
        userService.createAndSaveUser(userDTO, mockHttpServletResponse);
    }

    @Test
    public void addBlackListToken() {
        BlackListToken token = new BlackListToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg",
                new Date(System.currentTimeMillis() + 14400 * 1000));

        BlackListToken result = userService.saveBlackListToken(token);
        assertEquals(token, result);
    }

    @Test
    public void addWrongBlackListToken() {
        BlackListToken token = new BlackListToken(
                "eyJhbGciOiJIUzUxMiJ9.&&&oGNgZ6r8n6iC2CGIbtxbg$$",
                null);

        exception.expect(ConstraintViolationException.class);
        userService.saveBlackListToken(token);
    }

    @Test
    public void addDuplicateBlackListToken() {
        BlackListToken token = new BlackListToken(
                "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg",
                new Date(System.currentTimeMillis() + 14400 * 1000));

        BlackListToken initialToken = userService.saveBlackListToken(token);
        assertEquals(token, initialToken);

        BlackListToken duplicateToken = userService.saveBlackListToken(initialToken);
        assertEquals(duplicateToken, initialToken);
    }

    @Test
    public void findUserByIdSuccessfully() {
        UserDTO correctuserDto = testDataUserDTO.getCorrectUserDTO();
        Long userId = Long.parseLong(correctuserDto.getBsn());
        User expectedUser = new User(correctuserDto);

        userService.createAndSaveUser(correctuserDto, new MockHttpServletResponse());
        User savedUser = userService.findUserById(userId);

        assertEquals(expectedUser.getBsn(), savedUser.getBsn());
        assertEquals(expectedUser.getZipCode(), savedUser.getZipCode());
        assertNotEquals(expectedUser.getPassword(), savedUser.getPassword());
        assertEquals(60, savedUser.getPassword().length());

        userService.deleteUser(savedUser, new MockHttpServletResponse());
    }

    @Test
    public void findUserByIdNotfound() {
        UserDTO correctuserDto = testDataUserDTO.getCorrectUserDTO();
        Long userId = Long.parseLong(correctuserDto.getBsn());
        new User(correctuserDto);

        exception.expect(EntityNotFoundException.class);
        userService.findUserById(userId);
    }

    @Test
    public void getDashBoardTiles() {
        UserDTO correctuserDto = testDataUserDTO.getCorrectUserDTO();
        User savedUser = userService.createAndSaveUser(correctuserDto, new MockHttpServletResponse());

        Set<Tileable> tiles = userService.getAllDashboardTiles(Long.parseLong(correctuserDto.getBsn()));

        assertEquals(2, tiles.size());

        userService.deleteUser(savedUser, new MockHttpServletResponse());
    }
}
