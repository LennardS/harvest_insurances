package com.user;

import com.dashboard.Tileable;
import com.dashboard.UserLoginTile;
import com.dashboard.UserProfileTile;
import com.helper.TestDataUserDTO;
import com.helper.TestDataUserDTOImpl;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.TransactionSystemException;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Mock
    private UserDAO userDAO;
    @Mock
    private BlackListTokenDAO blackListTokenDAO;
    @Mock
    private BCryptPasswordEncoder encoder;
    @InjectMocks
    private UserService userService = new UserService(userDAO, blackListTokenDAO);
    private TestDataUserDTO testDataUserDTO = new TestDataUserDTOImpl();
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        //set the password encoder manually as the 'real' password encoder is dependency injected from the application context
        userService.setBcryptEncoder(encoder);
    }

    @Test
    public void createFailedUserTest() {
        userService = Mockito.spy(userService);
        UserDTO failedUserDTO = testDataUserDTO.getFailedUserDTO();
        User failedUser = new User(failedUserDTO);

        ConstraintViolationException constraintEx = new ConstraintViolationException((new HashSet<>()));

        when(userDAO.save(any(User.class))).thenThrow(new TransactionSystemException("", constraintEx));
        when(userDAO.findById(failedUser.getBsn())).thenReturn(Optional.empty());

        try {
            userService.createAndSaveUser(failedUserDTO, new MockHttpServletResponse());
        } catch(TransactionSystemException ex) {
            verify(userService.getBcryptEncoder(), times(1)).encode(failedUserDTO.getPassword());
        }
    }

    @Test
    public void createPartiallyFailedUserTest() {
        userService = Mockito.spy(userService);
        UserDTO partiallyFailedUserDto = testDataUserDTO.getPartiallyFailedUserDTO();
        User partiallyFailedUser = new User(partiallyFailedUserDto);

        ConstraintViolationException constraintEx = new ConstraintViolationException((new HashSet<>()));
        TransactionSystemException transEx = new TransactionSystemException("", constraintEx);

        when(userDAO.findById(partiallyFailedUser.getBsn())).thenReturn(Optional.empty());
        when(userDAO.save(any(User.class))).thenThrow(transEx);

        try {
            userService.createAndSaveUser(partiallyFailedUserDto, new MockHttpServletResponse());
        } catch(TransactionSystemException ex) {
            verify(userService.getBcryptEncoder(), times(1)).encode(partiallyFailedUserDto.getPassword());
        }
    }

    @Test
    public void createEmptyUserTest() {
        userService = Mockito.spy(userService);
        UserDTO emptyUserDTO = testDataUserDTO.getEmptyUserDTO();

        exception.expect(NumberFormatException.class);
        userService.createAndSaveUser(emptyUserDTO, new MockHttpServletResponse());

        try {
            userService.createAndSaveUser(emptyUserDTO, new MockHttpServletResponse());
        } catch(NumberFormatException ex) {
            assertEquals("For input string: \"\"", ex.getMessage());
            verify(userDAO, never()).findById(any(Long.TYPE));
            verify(userDAO, never()).save(any(User.class));
            verify(userService.getBcryptEncoder(), never()).encode(emptyUserDTO.getPassword());
        }
    }

    @Test
    public void createDuplicateUserTest() {
        userService = Mockito.spy(userService);
        UserDTO correctUserDTO = testDataUserDTO.getCorrectUserDTO();
        User correctUser = new User(correctUserDTO);

        when(userDAO.findById(correctUser.getBsn())).thenReturn(Optional.of(correctUser));
        when(userDAO.save(any(User.class))).thenReturn(correctUser);

        try {
            userService.createAndSaveUser(correctUserDTO, new MockHttpServletResponse());
        } catch (DataIntegrityViolationException ex) {
            assertNotEquals("", ex.getMessage());
            verify(userService.getBcryptEncoder(), times(1)).encode(correctUser.getPassword());
        }
    }

    @Test
    public void createCorrectUserTest() {
        userService = Mockito.spy(userService);
        UserDTO correctUserDTO = testDataUserDTO.getCorrectUserDTO();
        User correctUser = new User(correctUserDTO);

        when(userDAO.findById(correctUser.getBsn())).thenReturn(Optional.empty());
        when(userDAO.save(any(User.class))).thenReturn(correctUser);

        User savedUser = userService.createAndSaveUser(correctUserDTO, new MockHttpServletResponse());

        verify(encoder, times(1)).encode(correctUser.getPassword());
        assertEquals(correctUser, savedUser);
    }

    @Test
    public void findUserByIdSuccessfully() {
        userService = Mockito.spy(userService);
        UserDTO correctuserDto = testDataUserDTO.getCorrectUserDTO();
        Long userId = Long.parseLong(correctuserDto.getBsn());
        User expectedUser = new User(correctuserDto);
        when(userDAO.findById(expectedUser.getBsn())).thenReturn(Optional.of(expectedUser));

        User user = userService.findUserById(userId);

        verify(userDAO, times(2)).findById(userId);
        assertEquals(userId, user.getBsn());
    }

    @Test
    public void findUserByIdNotFound() {
        userService = Mockito.spy(userService);
        UserDTO correctuserDto = testDataUserDTO.getCorrectUserDTO();
        Long userId = Long.parseLong(correctuserDto.getBsn());
        User expectedUser = new User(correctuserDto);
        when(userDAO.findById(expectedUser.getBsn())).thenReturn(Optional.empty());
        try {
            userService.findUserById(userId);
        } catch(EntityNotFoundException ex) {
            verify(userDAO, times(1)).findById(userId);
        }
    }

    //this tests passes on docker and building with maven
    @Test
    public void getAllDashboardTilesOk() {
        User expectedUser = new User(testDataUserDTO.getCorrectUserDTO());
        when(userDAO.findById(expectedUser.getBsn())).thenReturn(Optional.of(expectedUser));

        Set<Tileable> tiles = userService.getAllDashboardTiles(expectedUser.getBsn());
        Object[] tilesArray = tiles.toArray();

        assertEquals(2, tiles.size());
    }
}