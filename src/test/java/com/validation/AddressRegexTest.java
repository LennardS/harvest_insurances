package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class AddressRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        AddressValidator addressValidator = new AddressValidator();
        addressValidator.initialize(null);
        expectedPattern = addressValidator.getAddressPattern();
    }

    @Test
    public void addressLongCorrect() {
        assertTrue("Korte Nieuwstraat 13".matches(expectedPattern));
    }

    @Test
    public void addressShortCorrect() {
        assertTrue("Fregatstraat 5".matches(expectedPattern));
    }

    @Test
    public void addressAlternativeCorrect() {
        assertTrue("Korte Nieuwstraat 13A".matches(expectedPattern));
    }

    @Test
    public void addressTooShort() {
        assertFalse("korte d".matches(expectedPattern));
    }

    @Test
    public void addressTooLong() {
        assertFalse("korte nieuwstraat 13 abbaa".matches(expectedPattern));
    }

    @Test
    public void addressWrong() {
        assertFalse("wwwwwwww".matches(expectedPattern));
    }
}
