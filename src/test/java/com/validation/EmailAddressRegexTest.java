package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class EmailAddressRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        EmailValidator emailValidator = new EmailValidator();
        emailValidator.initialize(null);
        expectedPattern = emailValidator.getEmailPattern();
    }

    @Test
    public void emailCorrect() {
        assertTrue("lennard12@hotmail.nl".matches(expectedPattern));
    }

    @Test
    public void emailRandCharsCorrect() {
        assertTrue("s76Kld@example.com".matches(expectedPattern));
    }

    @Test
    public void emailRandInCorrect() {
        assertFalse("ddd88&&@454.j".matches(expectedPattern));
    }

    @Test
    public void emailNoAt() {
        assertFalse("lennardhotmail.nl".matches(expectedPattern));
    }

    @Test
    public void emailNoDot() {
        assertFalse("lennard@hotmailnl".matches(expectedPattern));
    }

    @Test
    public void emailTooLong() {
        assertFalse("lennard@hotmail.nllll".matches(expectedPattern));
    }

    @Test
    public void emailWithSpace() {
        assertFalse("lennard12@hotmail.nl djdj".matches(expectedPattern));
    }
}
