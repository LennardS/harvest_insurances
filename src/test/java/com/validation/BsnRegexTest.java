package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class BsnRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        BsnStringValidator bsnStringValidator = new BsnStringValidator();
        bsnStringValidator.initialize(null);
        expectedPattern = bsnStringValidator.getBsnPattern();
    }

    @Test
    public void bsnRegexTestCorrect() {
        assertTrue("023456789".matches(expectedPattern));
    }

    @Test
    public void bsnRegexTestCorrect2() {
        assertTrue("023456789".matches(expectedPattern));
    }

    @Test
    public void bsnRegexTestNonDigit() {
        assertFalse("3838jdjdj".matches(expectedPattern));
    }

    @Test
    public void bsnRegexTestTooShort() {
        assertFalse("232".matches(expectedPattern));
    }
}
