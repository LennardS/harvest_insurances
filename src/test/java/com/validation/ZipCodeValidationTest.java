package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class ZipCodeValidationTest {
    private String expectedPattern;

    @Before
    public void init() {
        ZipCodeValidator zipCodeValidator = new ZipCodeValidator();
        zipCodeValidator.initialize(null);
        expectedPattern = zipCodeValidator.getZipCodePattern();
    }

    @Test
    public void zipCodeCorrect() {
        assertTrue("4401JE".matches(expectedPattern));
    }

    @Test
    public void ZipCodeIncorrect() {
        assertFalse("3838jdjdj".matches(expectedPattern));
    }

    @Test
    public void ZipCodeDigits() {
        assertFalse("111111".matches(expectedPattern));
    }

    @Test
    public void ZipCodeChars() {
        assertFalse("dfsajd".matches(expectedPattern));
    }

    @Test
    public void ZipCodeTooShort() {
        assertFalse("4401J".matches(expectedPattern));
    }

    @Test
    public void ZipCodeTooLong() {
        assertFalse("4401JK33".matches(expectedPattern));
    }
}
