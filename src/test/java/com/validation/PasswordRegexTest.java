package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class PasswordRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        PasswordValidator passwordValidator = new PasswordValidator();
        passwordValidator.initialize(null);
        expectedPattern = passwordValidator.getPasswordPattern();
    }

    @Test
    public void passwordCorrect() {
        assertTrue("SLF4kj$5%".matches(expectedPattern));
    }

    @Test
    public void passwordTooShort() {
        assertFalse("SlF4%".matches(expectedPattern));
    }

    @Test
    public void passwordMissingSpecialChar() {
        assertFalse("SLF4j54kt".matches(expectedPattern));
    }

    @Test
    public void passwordMissingDigit() {
        assertFalse("SSLF%%jklk".matches(expectedPattern));
    }

    @Test
    public void passwordMissingCapital() {
        assertFalse("slf4j$com".matches(expectedPattern));
    }
}
