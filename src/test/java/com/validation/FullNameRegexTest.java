package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class FullNameRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        FullNameValidator fullNameValidator = new FullNameValidator();
        fullNameValidator.initialize(null);
        expectedPattern = fullNameValidator.getFullNamePattern();
    }

    @Test
    public void fullNameCorrect() {
        assertTrue("Lennard Slabbekoorn".matches(expectedPattern));
    }

    @Test
    public void fullNameLong() {
        assertTrue("Lennard van den Heuvel".matches(expectedPattern));
    }

    @Test
    public void fullNameDigit() {
        assertFalse("12312".matches(expectedPattern));
    }

    @Test
    public void fullNameTooShort() {
        assertFalse("Lennard".matches(expectedPattern));
    }
}
