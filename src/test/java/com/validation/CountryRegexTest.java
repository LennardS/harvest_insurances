package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class CountryRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        CountryValidator countryValidator = new CountryValidator();
        countryValidator.initialize(null);
        expectedPattern = countryValidator.getCountryPattern();
    }

    @Test
    public void countryCorrect() {
        assertTrue("Nederland".matches(expectedPattern));
    }

    @Test
    public void countryCorrectShortCode() {
        assertTrue("CZ".matches(expectedPattern));
    }

    @Test
    public void countryCorrectWhiteSpace() {
        assertTrue("Verenigde Arabische Emiraten".matches(expectedPattern));
    }

    @Test
    public void countryCorrectSpecial() {
        assertTrue("België".matches(expectedPattern));
    }

    @Test
    public void countryDigits() {
        assertFalse("232".matches(expectedPattern));
    }

    @Test
    public void countryWrong() {
        assertFalse("&&&&&&&&&&&&&&&&&".matches(expectedPattern));
    }

    @Test
    public void countryTooShort() {
        assertFalse("j".matches(expectedPattern));
    }
}
