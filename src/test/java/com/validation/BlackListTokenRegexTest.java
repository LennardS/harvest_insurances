package com.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(SpringRunner.class)
public class BlackListTokenRegexTest {
    private String expectedPattern;

    @Before
    public void init() {
        BlackListTokenValidator tokenValidator = new BlackListTokenValidator();
        tokenValidator.initialize(null);
        expectedPattern = tokenValidator.getBlackListTokenPattern();
    }

    @Test
    public void tokenCorrect() {
        assertTrue("eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4NDkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDjZSmoGNgZ6r8n6iC2CGIbtxbg".matches(expectedPattern));
    }

    @Test
    public void tokenTooShort() {
        assertFalse("eyJhbGciOiC2CGIbtxbg".matches(expectedPattern));
    }

    @Test
    public void tokenWrong() {
        assertFalse("%eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxMjM0NTY3ODkiLCJleHAiOjE1NzM4#DkwMDUsImlhdCI6MTU3MzgzMTAwNX0.KSiFkowtPksyWGqT8lNJ3JclaHbczYNZejNiMwDf8Qg3zFBKdCqT1r0emX1mDj&SmoGNgZ6r8n6iC2CGIbtxbg".matches(expectedPattern));
    }
}
