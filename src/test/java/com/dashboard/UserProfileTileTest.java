package com.dashboard;

import com.helper.TestDataUserDTO;
import com.helper.TestDataUserDTOImpl;
import com.user.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
public class UserProfileTileTest {
    private TestDataUserDTO testDataUserDTO = new TestDataUserDTOImpl();
    private User user;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        user = new User(testDataUserDTO.getCorrectUserDTO());
    }

    @Test
    public void createUserProfileTileOk() {
        UserProfileTile userProfileTile = new UserProfileTile(user);
        Tileable profileTile = userProfileTile.createDashboardTile();
        Map<String, String> profileTileData = profileTile.getTileData();

        assertThat(profileTile, instanceOf(UserProfileTile.class));
        assertEquals(user.getBsn().toString(), profileTileData.get("bsn"));
        assertEquals(user.getFullName(), profileTileData.get("fullName"));
        assertEquals(user.getAddress(), profileTileData.get("address"));
        assertEquals(user.getZipCode(), profileTileData.get("zipCode"));
        assertEquals(user.getCountry(), profileTileData.get("country"));
    }

    @Test
    public void createUserProfileTileNullUser() {
        exception.expect(NullPointerException.class);
        Tileable profileTile = user.createDashboardTile();
    }
}
