package com.dashboard;

import com.helper.TestDataUserDTO;
import com.helper.TestDataUserDTOImpl;
import com.user.User;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

@RunWith(SpringRunner.class)
public class UserLoginTileTest {
    private TestDataUserDTO testDataUserDTO = new TestDataUserDTOImpl();
    private User user;
    @Rule
    public final ExpectedException exception = ExpectedException.none();

    @Before
    public void init() {
        user = new User(testDataUserDTO.getCorrectUserDTO());
    }

    @Test
    public void createUserLoginTileOk() {
        UserLoginTile userLoginTile = new UserLoginTile(user);
        Tileable loginTile = userLoginTile.createDashboardTile();
        Map<String, String> loginTileData = loginTile.getTileData();

        assertThat(loginTile, instanceOf(UserLoginTile.class));
        assertEquals(user.getBsn().toString(), loginTileData.get("bsn"));
        assertEquals(user.getEmailAddress(), loginTileData.get("emailAddress"));
        assertEquals("", loginTileData.get("password"));
    }

    @Test
    public void createUserLoginTileNullUser() {
        exception.expect(NullPointerException.class);
        user.createDashboardTile();
    }
}
