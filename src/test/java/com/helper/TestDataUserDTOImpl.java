package com.helper;

import com.helper.TestDataUserDTO;
import com.user.UserDTO;

public class TestDataUserDTOImpl implements TestDataUserDTO {
    @Override
    public UserDTO getFailedUserDTO() {
        return new UserDTO("123", "TestName", "34", "test@testmail",
                "123456778", "45TT45", "testpassword");
    }

    @Override
    public UserDTO getCorrectUserDTO() {
        return new UserDTO("987654321", "Lennard Slabbekoorn","Test teststraat 45a",
                "test@hotmail.nl", "Nederland", "5555UU", "SLF4j##2022");
    }

    @Override
    public UserDTO getEmptyUserDTO() {
        return new UserDTO("", "", "", "", "", "", "");
    }

    @Override
    public UserDTO getPartiallyFailedUserDTO() {
        return new UserDTO("14523", "Jan Zonneberg", "4455",
                "testst@hotmail.nl", "Nederland", "Je4401", "lllp45");
    }
}
