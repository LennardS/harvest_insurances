package com.helper;

import com.user.UserDTO;

public interface TestDataUserDTO {
    UserDTO getFailedUserDTO();
    UserDTO getCorrectUserDTO();
    UserDTO getEmptyUserDTO();
    UserDTO getPartiallyFailedUserDTO();
}
