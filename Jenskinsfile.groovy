
        node {
           currentBuild.result = 'SUCCESS'  
           stage('Prepare build backend') {
              git 'https://gitlab.com/LennardS/harvest_insurances.git'
              echo "\033[32m Pulling with Git OK, pipeline status: ${currentBuild.result} -> continuing \033[0m"
           }
           
           stage('Build backend') {
                bat 'mvn -B -DskipTests clean package'
                echo "\033[32m Build OK, pipeline status: ${currentBuild.result} -> continuing \033[0m"
           }
          stage('Unit tests backend') {
              try {
                bat 'mvn -X test'
                echo "\033[32m Unit tests passed, pipeline status: ${currentBuild.result} -> continuing \033[0m"
              } catch(ex) {
                currentBuild.result = 'UNSTABLE'
                echo "\033[33m Unit test failures detected, pipeline status: ${currentBuild.result} -> continuing \033[0m"
              }
          }
          
          stage('Integration tests backend') {
              try {
                  bat 'mvn -X verify'
                  echo "\033[32m Integration tests passed, pipeline status: ${currentBuild.result} -> continuing \033[0m"
              } catch(ex) {
                currentBuild.result = 'UNSTABLE'
                echo "\033[33m Integration test failures detected, pipeline status: ${currentBuild.result} -> continuing \033[0m"
              }
          }
    
          stage('Generate test reports backend') {
            junit 'target/*-reports/*.xml'
            echo "\033[32m Generating test reports OK, pipeline status: ${currentBuild.result} -> continuing \033[0m"
        }

        stage('Prepare build frontend') {
          bat 'cd client & '+ 
          'npm install --no-optional --quiet'
          echo "\033[32m Preparing frontend build OK, pipeline status: ${currentBuild.result} -> continuing \033[0m"
        }
        
        stage('build frontend') {
            bat 'cd client & npm rebuild node-sass & npm run build'
            echo "\033[32m Build OK, pipeline status: ${currentBuild.result} -> continuing \033[0m"
        }

        stage('Unit test frontend') {
          bat 'cd client & npm run test:unit'
          echo "\033[32m Unit tests passed, pipeline status: ${currentBuild.result} -> continuing \033[0m"
        }

        stage('Integration test frontend') {
            parallel 'Run frontend': {
              node {
                stage('Run Frontend') {
                  bat 'xcopy  ..\\Harvest-insurances-backend\\client /E /H /C'
                  bat 'npm run serve &'
                }
              }
            }, 'Run Cypress': {
              node {
                stage('Run Cypress') {
                  bat 'xcopy  ..\\Harvest-insurances-backend\\client /E /H /C'
                  bat 'npm run cypress:ci'
                  def hi = Hudson.instance
                  def pname = env.JOB_NAME.split('/')[0]

                  hi.getItem(pname).getItem(env.JOB_BASE_NAME).getBuilds().each{ build ->
                    def exec = build.getExecutor()

                    if (build.number != currentBuild.number && exec != null) {
                      exec.interrupt(
                        Result.ABORTED,
                        new CauseOfInterruption.UserInterruption(
                          "Aborted by #${currentBuild.number}"
                        )
                      )
                      println("Aborted previous running build #${build.number}")
                    } else {
                      println("Build is not running or is current build, not aborting - #${build.number}")
                    }
                  }
                }
              }
            }
          }

      stage('Deploy in Docker with Docker-compose') {
            try {
                bat 'docker-machine start default'
                echo 'Starting Docker Daemon'
            } catch(ex) {
                echo 'Docker Daemon has already been started -> using current Docker Deamon'
            }
          
            try {
                bat 'docker-compose up -d --build'
                echo  "\033[32m Deployment in Docker OK, pipeline status ${currentBuild.result} -> continuing \033[0m"
            } catch(ex) {
              currentBuild.result = 'FAILURE'
              echo "\033[31m  Docker build failed, pipeline status: ${currentBuild.result} -> continuing \033[0m" 
            }
            bat 'docker-compose down'
        }

        stage('Cleanup Workspace') {
          cleanWs notFailBuild: true
        }
    }

    //https://stackoverflow.com/questions/46834998/scripted-jenkinsfile-parallel-stage